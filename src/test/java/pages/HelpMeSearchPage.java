package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

public class HelpMeSearchPage {

    public HelpMeSearchPage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(xpath = "//div[@class='leftSide']/h1")
    public WebElement header1;

    @FindBy(css = "h3[role='presentation']")
    public WebElement header2;

    @FindBy(linkText = "TRY CAR FINDER")
    public WebElement tryCarFinder;
}
