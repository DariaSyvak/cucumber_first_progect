package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

public class AutoLoanCalculatorPage {
    public AutoLoanCalculatorPage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }
    @FindBy(xpath ="//input[@name='vehiclePrice']" )
    public WebElement inputPrice;

    @FindBy(id ="creditBlock")
    public WebElement creditScoreBlock;

    @FindBy(xpath = "//select[@name='loanTerm']")
    public WebElement loanTerm;

    @FindBy(xpath = "//input[@name='downPayment']")
    public WebElement downPayment;

    @FindBy(xpath = "//div[@class='loan-calculator-display-value']")
    public WebElement loanValue;

}

