package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class CarvanaHomePage {
    public CarvanaHomePage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }
    @FindBy(xpath = "//a[@data-cv-test='headerCarFinderLink']")
    public WebElement carFinderButton;

    @FindBy(xpath ="//a[@data-cv-test='headerTradesLink']")
    public WebElement sellAndTradeButton;

    @FindBy(css="div[data-cv-test='headerFinanceDropdown']")
    public WebElement financeButton;

    @FindBy(css = "a[data-cv-test='headerFinanceLoanCalc']")
    public WebElement autoLoanLocator;
}
