package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.concurrent.TimeUnit;

public class Driver {

    // 1. Private constructor -> no one can create the object of this class.
    private Driver() {

    }
    // 2. Private driver -> so no one can change and modify our web driver.
    private static WebDriver driver;

    // 3. Create methods for initialising and getting the driver then quiting driver
    public static WebDriver getDriver() {
        // if it is not initialised which means there is nothing inside it (not existing)
        if (driver == null) {

            //String browser = "chrome"; // define which browser you will run your test in
            // String browser = "firefox";
            // String browser = "safari";

            switch (ConfigReader.getProperty("browser")) {
                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                case "opera":
                    WebDriverManager.operadriver().setup();
                    driver = new OperaDriver();
                    break;
                case "firefox":
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                case "safari":
                    WebDriverManager.getInstance(SafariDriver.class).setup();
                    driver = new SafariDriver();
                    break;
                case "headless":
                    driver = new HtmlUnitDriver();
                    break;
                default:
                    throw new NotFoundException("Browser IS NOT DEFINED properly!!!");
            }
            // every web application is designed for fullscreen
            if (!ConfigReader.getProperty("browser").equals("headless")) {
                driver.manage().window().maximize();
                // for synchronizing our script to not get element not found exception (CHECKS FOR EXISTING)
                driver.manage().timeouts().implicitlyWait(Long.parseLong(ConfigReader.getProperty("implicitWait")), TimeUnit.SECONDS);
            }
        }
        return driver;
    }

        public static void quitDriver () {
//        try {
//            Thread.sleep(3000);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
            if (driver != null) {
                driver.manage().deleteAllCookies();
                driver.quit();
                driver = null;
            }
        }


    }

