package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pages.*;
import utils.Driver;
import utils.DropdownHandler;
import utils.Waiter;

public class CarvanaSteps {
    WebDriver driver;
    CarvanaHomePage carvanaHomePage;
    HelpMeSearchPage helpMeSearchPage;
    HelpMeSearchCarPage helpMeSearchCarPage;
    SellMyCarPage sellMyCarPage;
    AutoLoanCalculatorPage autoLoanCalculatorPage;


    @Before
    public void setup() {
        driver = Driver.getDriver();
        carvanaHomePage = new CarvanaHomePage();
        helpMeSearchPage = new HelpMeSearchPage();
        helpMeSearchCarPage = new HelpMeSearchCarPage();
        sellMyCarPage = new SellMyCarPage();
        autoLoanCalculatorPage = new AutoLoanCalculatorPage();

    }

    @Given("user is on {string}")
    public void userIsOn(String url) {
        driver.get(url);
    }

    @When("user clicks on {string} menu item")
    public void userClicksOnMenuItem(String button) {
        switch (button) {
            case ("CAR FINDER"):
                carvanaHomePage.carFinderButton.click();
                break;
            case "SELL/TRADE":
                Waiter.waitForVisibilityOfElement(driver,carvanaHomePage.carFinderButton,5);
                carvanaHomePage.sellAndTradeButton.click();
                break;
            case "AUTO LOAN CALCULATOR":
                carvanaHomePage.autoLoanLocator.click();
                break;


        }
    }

    @Then("user should be navigated to {string}")
    public void userShouldBeNavigatedTo(String url) {
        switch (url) {
            case "https://www.carvana.com/help-me-search/":
                Assert.assertEquals(url, driver.getCurrentUrl());
                break;
            case "https://www.carvana.com/help-me-search/qa":
                Assert.assertEquals(url, driver.getCurrentUrl());
                break;
            case "https://www.carvana.com/sell-my-car":
                Assert.assertEquals(url, driver.getCurrentUrl());
                break;

        }
    }

    @And("user should see {string} text")
    public void userShouldSeeText(String headingText) {
        switch (headingText) {
            case "WHAT CAR SHOULD I GET?":
                Assert.assertTrue(helpMeSearchPage.header1.isDisplayed());
                Assert.assertEquals(headingText, helpMeSearchPage.header1.getText());
                break;
            case "Car Finder can help! Answer a few quick questions to find the right car for you.":
                Assert.assertTrue(helpMeSearchPage.header2.isDisplayed());
                Assert.assertEquals(headingText, helpMeSearchPage.header2.getText());
                break;
            case "WHAT IS MOST IMPORTANT TO YOU IN YOUR NEXT CAR?":
                Assert.assertTrue(helpMeSearchCarPage.heading1.isDisplayed());
                Assert.assertEquals(headingText, helpMeSearchCarPage.heading1.getText());
                break;
            case "Select up to 4 in order of importance":
                Assert.assertTrue(helpMeSearchCarPage.heading2.isDisplayed());
                Assert.assertEquals(headingText, helpMeSearchCarPage.heading2.getText());
                break;
            case "GET A REAL OFFER IN 2 MINUTES":
                Assert.assertTrue(sellMyCarPage.heading1.isDisplayed());
                Assert.assertEquals(headingText, sellMyCarPage.heading1.getText());
                break;
            case "We pick up your car. You get paid on the spot.":
                Assert.assertTrue(sellMyCarPage.heading2.isDisplayed());
                Assert.assertEquals(headingText, sellMyCarPage.heading2.getText());
                break;
//            case "We couldn’t find that VIN. Please check your entry and try again.":
//                Assert.assertTrue(sellMyCarPage.heading2.isDisplayed());
//                Assert.assertEquals(headingText, sellMyCarPage.heading2.getText());


        }

    }

    @And("user should see {string} link")
    public void userShouldSeeLink(String tryLink) {
        Assert.assertTrue(helpMeSearchPage.tryCarFinder.isDisplayed());
        Assert.assertEquals(tryLink, helpMeSearchPage.tryCarFinder.getText());
    }


    @When("user clicks on {string} link")
    public void userClicksOnLink(String tryLinkButton) {
        helpMeSearchPage.tryCarFinder.click();

    }

    @When("user clicks on {string} button")
    public void userClicksOnButton(String button) {
        switch (button) {
            case "VIN":
                sellMyCarPage.vinButton.click();
                break;
            case "GET MY OFFER":
                sellMyCarPage.offerButton.click();

        }
    }

    @And("user enters vin number as {string}")
    public void userEntersVinNumberAs(String input) {
        sellMyCarPage.vinInput.isDisplayed();
        sellMyCarPage.vinInput.sendKeys(input);
    }

    @When("user hovers over on {string} menu item")
    public void userHoversOverOnMenuItem(String financeButton) {
        Waiter.waitForVisibilityOfElement(driver, carvanaHomePage.carFinderButton, 5);
        Actions actions = new Actions(driver);
        actions.moveToElement(carvanaHomePage.financeButton).perform();

    }

    @When("user enters {string} as {string}")
    public void userEntersAs(String inputBox, String value) {
        switch (inputBox) {
            case "Cost of Car I want":
                autoLoanCalculatorPage.inputPrice.sendKeys(value);
                break;
            case "What is Your Down Payment?":
                autoLoanCalculatorPage.downPayment.sendKeys(value);
                break;
        }
    }

    @And("user selects {string} as {string}")
    public void userSelectsAs(String box, String value) {
        switch (box) {
            case "What’s Your credit Score?":
                DropdownHandler.selectOptionByVisibleText(autoLoanCalculatorPage.creditScoreBlock, value);
                break;
            case "Choose Your Loan Terms":
                DropdownHandler.selectOptionByVisibleText(autoLoanCalculatorPage.loanTerm, value);
                break;
        }
    }

    @Then("user should see the monthly payment as {string}")
    public void userShouldSeeTheMonthlyPaymentAs(String arg) {
        Assert.assertTrue(autoLoanCalculatorPage.loanValue.isDisplayed());
        Assert.assertEquals(arg, autoLoanCalculatorPage.loanValue.getText());
    }
}